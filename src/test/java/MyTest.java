import org.junit.Test;

import static org.junit.Assert.*;

public class MyTest {

    @Test
    public void testGreeting() {
        GreetingsEN myUnit = new GreetingsEN();

        assertEquals("Hello", myUnit.sayHello());

    }
}
